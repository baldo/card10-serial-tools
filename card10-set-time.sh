#!/usr/bin/env bash

set -e

(
    echo -n $'\cc'
    echo -n $'\ca'
    echo -e '\r'
    echo -n $'\ce'

    echo -ne 'import utime\r'
    echo -ne "utime.set_unix_time("$(date +%s)")\r"

    echo -ne 'import os\r'
    echo -ne 'os.exit()\r'

    echo -n $'\cd'

    sleep 3
) | picocom -q -b 115200 /dev/ttyACM0
