#!/usr/bin/env bash

set -e

(
    echo -n $'\cc'
    echo -n $'\ca'
    echo -e '\r'
    echo -n $'\ce'

    echo -ne 'import os\r'
    echo -ne 'os.reset()\r'

    echo -n $'\cd'

    sleep 3
) | picocom -q -b 115200 /dev/ttyACM0
