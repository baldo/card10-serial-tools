About
=====

Some usefull tools for interacting with card10 via a serial connection.

At the moment the scripts expect card10 to be available via `/dev/ttyACM0`.

Dependencies
============

* bash
* picocom

