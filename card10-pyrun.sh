#!/usr/bin/env bash

set -e

(
    echo -n $'\cc'
    echo -n $'\ca'
    echo -e '\r'
    echo -n $'\ce'

    cat "$1" | tr '\n' '\r'

    echo -n $'\cd'

    sleep 3
) | picocom -q -b 115200 /dev/ttyACM0
